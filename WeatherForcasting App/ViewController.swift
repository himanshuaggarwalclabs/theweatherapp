//
//  ViewController.swift
//  WeatherForcasting App
//
//  Created by Samar Singla on 21/01/15.
//  Copyright (c) 2015 Gupta&sons. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITextFieldDelegate{
    
    
    @IBOutlet weak var yourCity: UITextField!    // TextField to get City
    @IBOutlet weak var weatherDisplay: UILabel! // Label for Displaying your City Weather
    @IBOutlet weak var cityLabel: UILabel!     // Label for Displaying your City
    @IBOutlet weak var maxTemp: UILabel!      // Label for Displaying Maximum Temprature
    @IBOutlet weak var minTemp: UILabel!     // Label for Displaying Minimum Temprature
    
    @IBAction func getCity(sender: AnyObject) {
       // String Variable used to geting URL of weather forecasting Website for selected City
       var urlString = "http://www.weather-forecast.com/locations/" + yourCity.text.stringByReplacingOccurrencesOfString(" ", withString:"") + "/forecasts/latest"
       var url = NSURL(string : urlString)
       let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {
                  (data,response,error) in
                  var urlContent = NSString(data: data, encoding:NSUTF8StringEncoding)
                  // for geting City Weather from www.weather- forecast.com
                 // and a Condition for check if City is Valid
                  if NSString(string: urlContent!).containsString("<span class=\"phrase\">") {
                    // for storing City Weather in Variable
                    var contentArray = urlContent!.componentsSeparatedByString("<span class=\"phrase\">")
                    var newcontentArray = contentArray[1].componentsSeparatedByString("</span>")
                    var weatherForcast = newcontentArray[0].stringByReplacingOccurrencesOfString("&deg;", withString: "º")
                    // for storing Maximum Temprature in Variable
                    var maxtempArray = weatherForcast.componentsSeparatedByString("max")
                    var newmaxtempArray = maxtempArray[1].componentsSeparatedByString("on")
                    // for storing Minimum Temprature in Variable
                    var mintempArray = weatherForcast.componentsSeparatedByString("min")
                    var newmintempArray = mintempArray[1].componentsSeparatedByString("on")
                        // it Run  this task in Background by seting its priority on Top
                        dispatch_async(dispatch_get_main_queue()) {
                        self.cityLabel.text = self.yourCity.text.uppercaseString
                        self.weatherDisplay.text = weatherForcast
                        self.maxTemp.text = "\(newmaxtempArray[0])\n Max"
                        self.minTemp.text = "\(newmintempArray[0])\n Min"
                        }
                  // its a Condition if City is Not Valid
                } else {
                      dispatch_async(dispatch_get_main_queue()) {
                      self.weatherDisplay.text = " oops! could not find that city -- plz.. try again"
                      self.cityLabel.text = " "
                      self.maxTemp.text = " "
                      self.minTemp.text = " "
                      } // Dispatch Closed
                 } // esle closed
            } // URL Session Closed
      task.resume()
      self.view.endEditing(true)                                                                 // Button for get your City  Weather
      } // Button function Closed
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        }
    override func didReceiveMemoryWarning() {
             super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override  func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
              self.view.endEditing(true)                                                      // KeyBoard Manging Function
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
         yourCity.resignFirstResponder()
         return true                                                                         // KeyBoard Managing Function
    }
} // ViewController Closed

